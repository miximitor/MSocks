//
// Created by maxtorm on 18-7-19.
//

#include "server_session.h"
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <glog/logging.h>

using namespace boost::asio;
using boost::system::error_code;
using namespace ip;

static inline std::size_t generate_id()
{
  static std::size_t id = 0;
  return id++;
}

server_session::server_session(tcp::socket local_socket, const msocks::secret_t &secret) :
  local_socket_(std::move(local_socket)),
  remote_socket_(local_socket_.get_executor().context()),
  secret_(secret),
  timer_(local_socket_.get_executor().context()),
  resolver_(local_socket_.get_executor().context()),
  id_(generate_id())
{
  local_read_buffer_1_.reserve(msocks::get_pack_size(BUFFER_SIZE));
  remote_read_buffer_1_.reserve(BUFFER_SIZE);
}

void server_session::run()
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " read from local socket error(1):" << ec.message();
      close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    handshake_1();
  };
  local_socket_.async_receive(buffer(local_read_buffer_0_), read);
  
}

void server_session::handshake_1()
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " read from local socket error(1):" << ec.message();
      close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    handshake_1();
  };
  
  request.clear();
  request.resize(msocks::get_unpack_size(local_read_buffer_1_.size()));
  auto n = msocks::msg_unpack(local_read_buffer_1_.data(), local_read_buffer_1_.size(), request.data(), secret_);
  if ( n == 0 )
  {
    local_read_buffer_1_.clear();
    connect_remote();
  }
  else if ( n < 0 )
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
  }
  else
  {
    LOG(ERROR) << id_ << " invalid message pack";
    close_all();
    return;
  }
}

void server_session::connect_remote()
{
  auto session = shared_from_this();
  auto pos = request.find_last_of(':');
  auto host = request.substr(0, pos);
  auto service = request.substr(pos + 1);
  auto write_cb = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " write to local socket error: " << ec.message();
      close_all();
      return;
    }
    
    data_handle();
  };
  
  auto connect_cb = [session, this, write_cb](const error_code &ec)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " connect to " << request << " error: " << ec.message();
      close_all();
      return;
    }
    auto digest = msocks::sha256(request);
    memcpy(local_write_buffer_.data(), digest.data(), digest.size());
    async_write(local_socket_, buffer(local_write_buffer_, digest.size()), write_cb);
  };
  
  auto resolve_cb = [session, this, connect_cb](const error_code &ec, tcp::resolver::results_type results)
  {
    auto ep = *results.begin();
    if ( ec )
    {
      LOG(ERROR) << id_ << " resolve error: " << ec.message();
      close_all();
      return;
    }
    
    remote_socket_.async_connect(ep, connect_cb);
  };
  
  resolver_.async_resolve(host, service, resolve_cb);
}


void server_session::data_handle()
{
  auto session = shared_from_this();
  auto local_read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    on_local_read();
  };
  
  auto remote_read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    on_remote_read();
  };
  
  local_socket_.async_receive(buffer(local_read_buffer_0_), local_read);
  
  remote_socket_.async_receive(buffer(remote_read_buffer_0_), remote_read);
}

void server_session::on_local_read()
{
  auto session = shared_from_this();
  
  auto write = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " write to remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    on_remote_write(n_write);
  };
  
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    on_local_read();
  };
  
  auto n = msocks::msg_unpack(
    local_read_buffer_1_.data(),
    local_read_buffer_1_.size(),
    remote_write_buffer_.data(),
    secret_
  );
  
  if ( n >= 0 )
  {
    auto to_write = msocks::get_unpack_size(local_read_buffer_1_.size() - n);
    async_write(remote_socket_, buffer(remote_write_buffer_, to_write), write);
  }
  else
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
  }
}

void server_session::on_local_write()
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    on_remote_read();
  };
  
  remote_read_buffer_1_.clear();
  remote_socket_.async_receive(buffer(remote_read_buffer_0_), std::move(read));
}

void server_session::on_remote_read()
{
  auto session = shared_from_this();
  auto write = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " write to local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    on_local_write();
  };
  
  msocks::msg_pack(
    remote_read_buffer_1_.data(),
    remote_read_buffer_1_.size(),
    local_write_buffer_.data(),
    secret_
  );
  
  auto to_write = msocks::get_pack_size(remote_read_buffer_1_.size());
  async_write(local_socket_, buffer(local_write_buffer_, to_write), std::move(write));
}

void server_session::on_remote_write(std::size_t n_write)
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    on_local_read();
  };
  
  consume(msocks::get_pack_size(n_write), local_read_buffer_1_);
  if ( !local_read_buffer_1_.empty())
  {
    on_local_read();
  }
  else
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
  }
  
}

void server_session::consume(size_t n, std::vector<uint8_t> &target)
{
  if ( n == target.size())
  {
    target.clear();
  }
  else
  {
    std::move(target.begin() + n, target.end(), target.begin());
    while ( n-- > 0 ) target.pop_back();
  }
}

void server_session::close_all()
{
  error_code ec;
  local_socket_.close(ec);
  remote_socket_.close(ec);
  timer_.cancel(ec);
  resolver_.cancel();
}

server_session::~server_session()
{
  LOG(INFO) << id_ << " session released";
}
