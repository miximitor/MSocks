//
// Created by maxtorm on 18-7-16.
//

#ifndef MSOCKS_BASIC_SERVER_H
#define MSOCKS_BASIC_SERVER_H

#include "Encrypt.h"
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/noncopyable.hpp>
class basic_server : public boost::noncopyable
{
protected:
  boost::asio::io_context io_context_;
  boost::asio::ip::tcp::acceptor acceptor_;
  msocks::secret_t secret_;
  
  basic_server(
    boost::asio::ip::tcp::endpoint local_ep_,
    std::string_view secret
  );
  void run();
};


#endif //MSOCKS_BASIC_SERVER_H
