//
// Created by maxtorm on 18-7-11.
//
#include <string_view>
#include <glog/logging.h>
#include "server.h"
#include "server_session.h"

using namespace boost::asio;
using boost::system::error_code;
using namespace ip;

server::server(tcp::endpoint local_ep, std::string_view secret) :
  basic_server(local_ep, secret)
{
}

void server::start()
{
  acceptor_.async_accept([this](const error_code &ec, tcp::socket ps)
                         {
                           on_accept(ec, std::move(ps));
                         });
  basic_server::run();
}

void server::on_accept(const error_code &ec, tcp::socket ps)
{
  if ( ec )
  {
    LOG(FATAL) << "on client accepting connection error: " << ec.message();
  }
  
  std::shared_ptr<server_session> session(new server_session(std::move(ps), secret_));
  session->run();
  acceptor_.async_accept([this](const error_code &ec, tcp::socket ps)
                         {
                           on_accept(ec, std::move(ps));
                         });
}
