//
// Created by maxtorm on 18-7-17.
//

#ifndef MSOCKS_CLIENT_SESSION_H
#define MSOCKS_CLIENT_SESSION_H


#include <memory>
#include <array>
#include <boost/noncopyable.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/deadline_timer.hpp>

#include "Encrypt.h"

class client_session : public std::enable_shared_from_this<client_session>,
                       public boost::noncopyable
{
private:
  
  constexpr static int BUFFER_SIZE = 65536;
  constexpr static int CONNECTION_TIMEOUT_TIME = 600;
  constexpr static uint8_t SOCKS_VERSION_5 = 0x05;
  constexpr static uint8_t ANONYMOUS_METHOD = 0x00;
  constexpr static uint8_t CONNECT_CMD = 0x01;
  
  constexpr static uint8_t IPV4_ATYP = 0x01;
  constexpr static auto IPV4_LEN = sizeof(uint32_t);
  constexpr static uint8_t DOMAIN_ATYP = 0x03;
  constexpr static uint8_t IPV6_ATYP = 0x04;
  constexpr static auto IPV6_LEN = 16;
  constexpr static auto PORT_LEN = sizeof(uint16_t);
  
  boost::asio::ip::tcp::socket local_socket_;
  boost::asio::ip::tcp::socket remote_socket_;
  
  std::array<uint8_t, BUFFER_SIZE> local_read_buffer_0_;
  std::array<uint8_t, msocks::get_pack_size(BUFFER_SIZE)> remote_read_buffer_0_;
  
  std::array<uint8_t, msocks::get_pack_size(BUFFER_SIZE)> remote_write_buffer_;
  std::array<uint8_t, BUFFER_SIZE> local_write_buffer_;
  
  std::vector<uint8_t> local_read_buffer_1_;
  std::vector<uint8_t> remote_read_buffer_1_;
  
  const msocks::secret_t &secret_;
  const boost::asio::ip::tcp::endpoint &remote_ep_;
  std::string remote_request_endpoint_;
  
  boost::asio::deadline_timer timer_;
  
  uint64_t id_;

public:
  client_session(
    boost::asio::ip::tcp::socket local_socket,
    const msocks::secret_t &secret,
    const boost::asio::ip::tcp::endpoint &remote_ep
  );
  
  void run();
  
  ~client_session();

private:
  
  
  void handshake_1();
  
  void handshake_2();
  
  void handshake_3();
  
  void data_handle();
  
  void on_local_read();
  
  void on_local_write(std::size_t n_write);
  
  void on_remote_read();
  
  void on_remote_write();
  
  void connect_remote();
  
  void close_all();
  void consume(size_t n, std::vector<uint8_t> &target);
};


#endif //MSOCKS_CLIENT_SESSION_H
