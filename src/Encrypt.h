//
// Created by maxtorm on 18-6-18.
//

#ifndef MSOCKS_HASH_H
#define MSOCKS_HASH_H

#include <cryptopp/secblock.h>
#include <cryptopp/sha.h>
#include <vector>

namespace msocks
{
using msg_length_t = uint32_t;
using secret_t = CryptoPP::SecByteBlock;

constexpr int MSG_HEADER_SIZE = sizeof(msg_length_t);
constexpr int KEY_LEN = CryptoPP::SHA256::DIGESTSIZE;
constexpr int IV_LEN = 128 / 8;

secret_t sha256(std::string_view origin);

constexpr size_t get_pack_size(std::size_t data_sz)
{
  return MSG_HEADER_SIZE + IV_LEN + data_sz;
}

constexpr size_t get_unpack_size(std::size_t data_sz)
{
  if ( data_sz > (MSG_HEADER_SIZE + IV_LEN))
    return data_sz - MSG_HEADER_SIZE - IV_LEN;
  else
    return 0;
}

ssize_t msg_pack(
  const void *input,
  std::size_t input_sz,
  void *output,
  const secret_t &secret
);

ssize_t msg_unpack(
  const void *input,
  std::size_t input_sz,
  void *output,
  const secret_t &secret
);

}

#endif //MSOCKS_HASH_H
