//
// Created by maxtorm on 18-7-16.
//

#include "basic_server.h"

basic_server::basic_server (
    boost::asio::ip::tcp::endpoint local_ep_,
    std::string_view secret
  ) :
  io_context_(),
  acceptor_(io_context_,local_ep_),
  secret_(msocks::sha256(secret))
{
}

void basic_server::run()
{
 io_context_.run();
}
