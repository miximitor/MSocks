#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <glog/logging.h>
#include <tuple>
#include "server.h"
#include "client.h"

using boost::asio::io_context;
using boost::asio::ip::tcp;
using namespace boost::asio::ip;

std::tuple<std::string, std::string, uint16_t, std::string>
parse_arg(int argc, char *argv[]);


int main(int argc, char *argv[])
{
  google::InitGoogleLogging(argv[0]);
  FLAGS_colorlogtostderr = true;
  FLAGS_stderrthreshold = 0;
  auto[type, ip, port, psk] = parse_arg(argc, argv);
  if ( type == "server" )
  {
    tcp::endpoint ep(make_address_v4(ip), port);
    server s(ep, psk);
    s.start();
  }
  else if ( type == "client" )
  {
    tcp::endpoint ep(make_address_v4(ip), port);
    client c(ep, psk);
    c.start();
  }
  puts("Usage server|client ip port password");
  return -1;
}

std::tuple<std::string, std::string, uint16_t, std::string> parse_arg(int argc, char **argv)
{
  if ( argc != 4 + 1 )
    return std::make_tuple<std::string, std::string, uint16_t, std::string>("", "", 0, "");
  return std::make_tuple<std::string, std::string, uint16_t, std::string>(argv[1],
                                                                          argv[2],
                                                                          (uint16_t) std::atoi(argv[3]),
                                                                          argv[4]);
}
