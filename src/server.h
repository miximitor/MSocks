//
// Created by maxtorm on 18-7-11.
//

#ifndef MSOCKS_SERVER_H
#define MSOCKS_SERVER_H

#include <boost/asio/ip/tcp.hpp>
#include "basic_server.h"

class server : public basic_server
{
private:
public:
  server(boost::asio::ip::tcp::endpoint local_ep,std::string_view secret);
  void start();
private:
  void on_accept(const boost::system::error_code &ec,boost::asio::ip::tcp::socket ps);
};

#endif //MSOCKS_SERVER_H
