//
// Created by maxtorm on 18-7-19.
//

#ifndef MSOCKS_SERVER_SESSION_H
#define MSOCKS_SERVER_SESSION_H


#include <memory>
#include <vector>
#include "Encrypt.h"
#include <boost/asio/ip/tcp.hpp>
#include <boost/noncopyable.hpp>
#include <boost/asio/deadline_timer.hpp>

class server_session : public std::enable_shared_from_this<server_session>,
                       public boost::noncopyable
{
private:
  constexpr static int BUFFER_SIZE = 65536;
  boost::asio::ip::tcp::socket local_socket_;
  boost::asio::ip::tcp::socket remote_socket_;
  
  std::array<uint8_t, msocks::get_pack_size(BUFFER_SIZE)> local_read_buffer_0_;
  std::array<uint8_t, BUFFER_SIZE> remote_read_buffer_0_;
  
  std::array<uint8_t, BUFFER_SIZE> remote_write_buffer_;
  std::array<uint8_t, msocks::get_pack_size(BUFFER_SIZE)> local_write_buffer_;
  
  std::vector<uint8_t> local_read_buffer_1_;
  std::vector<uint8_t> remote_read_buffer_1_;
  
  const msocks::secret_t &secret_;
  boost::asio::deadline_timer timer_;
  boost::asio::ip::tcp::resolver resolver_;

  std::string request;
  std::size_t id_;
public:
  server_session(
    boost::asio::ip::tcp::socket local_socket,
    const msocks::secret_t &secret
  );
  void run();
  ~server_session();

private:
  
  
  void handshake_1();
  void connect_remote();
  
  void data_handle();
  
  void on_local_read();
  void on_local_write();
  void on_remote_read();
  void on_remote_write(std::size_t n_write);
  
  void close_all();
  
  void consume(size_t n, std::vector<uint8_t> &target);
};


#endif //MSOCKS_SERVER_SESSION_H
