//
// Created by maxtorm on 18-6-20.
//
#include "Encrypt.h"
#include <boost/endian/conversion.hpp>
#include <cryptopp/osrng.h>
#include <cryptopp/modes.h>
#include <cryptopp/sha.h>
#include <cryptopp/aes.h>
#include <assert.h>
#include <iostream>

namespace msocks
{


CryptoPP::SecByteBlock sha256(std::string_view origin)
{
  CryptoPP::SHA256 hs;
  auto size = CryptoPP::SHA256::DIGESTSIZE;
  CryptoPP::SecByteBlock digest(size);
  CryptoPP::byte *in = reinterpret_cast<CryptoPP::byte *>(const_cast<char *>(origin.data()));
  hs.CalculateDigest(digest.data(), in, origin.size());
  return digest;
}

ssize_t msg_pack(const void *input, std::size_t input_sz, void *output, const secret_t &secret)
{
  static CryptoPP::AutoSeededRandomPool rng;
  static CryptoPP::SecByteBlock iv(IV_LEN);
  static CryptoPP::CFB_Mode<CryptoPP::AES>::Encryption encrypt;
  const CryptoPP::byte *in = static_cast<const CryptoPP::byte *>(input);
  CryptoPP::byte *out = static_cast<CryptoPP::byte *>(output);
  
  //get message length
  msg_length_t msg_len = static_cast<msg_length_t>(IV_LEN + input_sz);
  // convert to big endian
  boost::endian::native_to_big_inplace(msg_len);
  
  rng.GenerateBlock(iv.data(), iv.size());
  encrypt.SetKeyWithIV(secret.data(), secret.size(), iv.data(), iv.size());
  
  memcpy(out, &msg_len, MSG_HEADER_SIZE);
  memcpy(out + MSG_HEADER_SIZE, iv.data(), IV_LEN);
  
  encrypt.ProcessData(out + MSG_HEADER_SIZE + IV_LEN,
                      in, input_sz);
  return 0;
}

ssize_t msg_unpack(const void *input, std::size_t input_sz, void *output, const secret_t &secret)
{
  static CryptoPP::CFB_Mode<CryptoPP::AES>::Decryption decrypt;
  static CryptoPP::SecByteBlock iv(IV_LEN);
  
  if ( input_sz < MSG_HEADER_SIZE )
    return -(MSG_HEADER_SIZE - input_sz);
  
  const CryptoPP::byte *in = static_cast<const CryptoPP::byte *>(input);
  CryptoPP::byte *out = static_cast<CryptoPP::byte *>(output);
  
  msg_length_t msg_len = 0;
  memcpy(&msg_len, in, MSG_HEADER_SIZE);
  boost::endian::big_to_native_inplace(msg_len);
  
  if ( input_sz - MSG_HEADER_SIZE < msg_len )
    return -(msg_len - (input_sz - MSG_HEADER_SIZE));
  
  ssize_t ret = 0;
  if ( input_sz - MSG_HEADER_SIZE > msg_len )
    ret = input_sz - (msg_len + MSG_HEADER_SIZE);
  
  memcpy(iv.data(), in + MSG_HEADER_SIZE, IV_LEN);
  
  decrypt.SetKeyWithIV(secret.data(), secret.size(), iv.data(), iv.size());
  
  decrypt.ProcessData(out,
                      in + MSG_HEADER_SIZE + IV_LEN,
                      msg_len - IV_LEN);
  return ret;
}


}














