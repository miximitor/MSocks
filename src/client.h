//
// Created by maxtorm on 18-7-12.
//

#ifndef MSOCKS_CLIENT_H
#define MSOCKS_CLIENT_H

#include "basic_server.h"

class client : public basic_server
{
private:
  boost::asio::ip::tcp::endpoint remote_ep_;
public:
  client (
    boost::asio::ip::tcp::endpoint remote_ep,
    std::string_view secret
  );
  
  void start();
private:
  
  void on_accept (
    const boost::system::error_code &ec,
    boost::asio::ip::tcp::socket ps
  );
};

#if 0
class client_session : public std::enable_shared_from_this<client_session>,
                       public boost::noncopyable
{
private:
  constexpr static std::size_t BUFFER_SIZE = 131072;
  constexpr static int CONNECTION_TIMEOUT_TIME = 600;

  boost::asio::ip::tcp::socket local_socket_;
  boost::asio::ip::tcp::socket remote_socket_;
  const boost::asio::ip::tcp::endpoint &remote_ep_;
  const CryptoPP::SecByteBlock &secret_;
  boost::asio::deadline_timer timer_;
  std::size_t id_;

  std::array<uint8_t, BUFFER_SIZE> local_buffer_0_;
  std::array<uint8_t, BUFFER_SIZE> remote_buffer_0_;

  int local_handshake_state = 0;

public:
  client_session(boost::asio::ip::tcp::socket local,
                 const CryptoPP::SecByteBlock &secret,
                 const boost::asio::ip::tcp::endpoint &remote_ep);

  void run();

private:
  void hand_shake_1(const boost::system::error_code &ec, std::size_t nr);

  void hand_shake_2(const boost::system::error_code &ec, std::size_t nr);

  void handle_data();

  void close_all();

  void on_connect(const boost::system::error_code &ec);

  void on_local_read(const boost::system::error_code &ec, std::size_t nr);

  void on_remote_read(const boost::system::error_code &ec, std::size_t nr);

  void on_local_write(const boost::system::error_code &ec, std::size_t nw);

  void on_remote_write(const boost::system::error_code &ec, std::size_t nw);
};
#endif

#endif //MSOCKS_CLIENT_H
