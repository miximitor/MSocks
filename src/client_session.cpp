//
// Created by maxtorm on 18-7-17.
//

#include "client_session.h"
#include <glog/logging.h>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/endian/conversion.hpp>

using namespace boost::asio;
using boost::system::error_code;
using namespace ip;

static inline std::size_t generate_id()
{
  static std::size_t id = 0;
  return id++;
}

client_session::client_session(
  tcp::socket local_socket,
  const msocks::secret_t &secret,
  const tcp::endpoint &remote_ep
) :
  local_socket_(std::move(local_socket)),
  remote_socket_(local_socket_.get_executor().context()),
  secret_(secret),
  remote_ep_(remote_ep),
  timer_(local_socket_.get_executor().context()),
  id_(generate_id())
{
  local_read_buffer_1_.reserve(BUFFER_SIZE);
  remote_read_buffer_1_.reserve(msocks::get_pack_size(BUFFER_SIZE));
}

void client_session::run()
{
  connect_remote();
}

void client_session::connect_remote()
{
  auto session = shared_from_this();
  
  
  auto remote_connect_cb = [session, this](const error_code &ec)
  {
    if ( ec )
    {
      if ( ec == boost::asio::error::operation_aborted )
        LOG(ERROR) << id_ << "connect to " << remote_ep_ << " connection timeout";
      else
        LOG(ERROR) << id_ << "connect to " << remote_ep_ << " error: " << ec.message();
      close_all();
      return;
    }
    timer_.cancel();
    handshake_1();
  };
  
  auto timer_expire_cb = [session, this](const error_code &ec)
  {
    if ( ec )
    {
      if ( ec != boost::asio::error::operation_aborted )
      {
        LOG(ERROR) << id_ << "timer wait failed,error: " << ec.message();
        close_all();
      }
      return;
    }
    LOG(ERROR) << "cancel";
    remote_socket_.cancel();
  };
  
  remote_socket_.async_connect(remote_ep_, remote_connect_cb);
  
  error_code ec;
  timer_.expires_from_now(boost::posix_time::milliseconds(CONNECTION_TIMEOUT_TIME), ec);
  if ( ec )
  {
    LOG(ERROR) << id_ << "timer setup failed error: " << ec.message();
    close_all();
    return;
  }
  
  timer_.async_wait(timer_expire_cb);
  
}

void client_session::handshake_1()
{
  auto session = shared_from_this();
  auto size = local_read_buffer_1_.size();
  
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " read from local socket error(1):" << ec.message();
      close_all();
      return;
    }
    local_read_buffer_1_.insert(local_read_buffer_1_.end(),
                                local_read_buffer_0_.begin(),
                                local_read_buffer_0_.begin() + n_read);
    handshake_1();
  };
  
  if ( size < 3 )
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
    return;
  }
  
  
  auto n_handle = 0;
  
  auto version = local_read_buffer_1_[n_handle++];
  if ( version != SOCKS_VERSION_5 )
  {
    LOG(ERROR) << id_ << " unsupported socks5 proxy version: " << version;
    close_all();
    return;
  }
  
  auto n_method = local_read_buffer_1_[n_handle++];
  if ( size - n_handle < n_method )
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
    return;
  }
  
  bool found_supported_method = false;
  for ( int i = 2; i < n_method + 2 && !found_supported_method; found_supported_method = (local_read_buffer_1_[i++] ==
                                                                                          ANONYMOUS_METHOD));
  n_handle += n_method;
  if ( !found_supported_method )
  {
    LOG(ERROR) << id_ << " supported authentication method not found.";
    close_all();
    return;
  }
  
  consume(n_handle, local_read_buffer_1_);
  
  if ( !local_read_buffer_1_.empty())
  {
    LOG(ERROR) << id_ << " invalid message pack(1)";
    close_all();
    return;
  }
  
  auto write_local_cb = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " write to local socket error(1):" << ec.message();
      close_all();
      return;
    }
    handshake_2();
  };
  
  local_write_buffer_[0] = SOCKS_VERSION_5;
  local_write_buffer_[1] = ANONYMOUS_METHOD;
  
  async_write(local_socket_, buffer(local_write_buffer_, 2), write_local_cb);
  
}

void client_session::handshake_2()
{
  auto session = shared_from_this();
  auto size = local_read_buffer_1_.size();
  
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " read from local socket error(2):" << ec.message();
      close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    handshake_2();
  };
  
  if ( size < 5 )
  {
    local_socket_.async_receive(buffer(local_read_buffer_0_), read);
    return;
  }
  
  auto n_handle = 0;
  auto version = local_read_buffer_1_[n_handle++];
  
  if ( version != SOCKS_VERSION_5 )
  {
    LOG(ERROR) << id_ << " unsupported socks5 proxy version: " << version;
    close_all();
    return;
  }
  
  auto cmd = local_read_buffer_1_[n_handle++];
  n_handle++;
  
  if ( cmd != CONNECT_CMD )
  {
    LOG(ERROR) << id_ << " unsupported command type: " << cmd;
    close_all();
    return;
  }
  
  auto atyp = local_read_buffer_1_[n_handle++];
  
  if ( atyp == IPV4_ATYP )
  {
    if ( size - n_handle < IPV4_LEN + PORT_LEN )
    {
      local_socket_.async_receive(buffer(local_read_buffer_0_), read);
      return;
    }
    uint32_t ipv4_address = 0;
    memcpy(&ipv4_address, &local_read_buffer_1_[n_handle], IPV4_LEN);
    n_handle += IPV4_LEN;
    boost::endian::big_to_native_inplace(ipv4_address);
    
    uint16_t port = 0;
    memcpy(&port, &local_read_buffer_1_[n_handle], PORT_LEN);
    n_handle += PORT_LEN;
    boost::endian::big_to_native_inplace(port);
    remote_request_endpoint_ = ip::make_address_v4(ipv4_address).to_string() + ":" + std::to_string(port);
  }
  else if ( atyp == DOMAIN_ATYP )
  {
    auto domain_len = local_read_buffer_1_[n_handle++];
    
    if ( size < n_handle + domain_len )
    {
      local_socket_.async_receive(buffer(local_read_buffer_0_), read);
      return;
    }
    
    std::string domain((const char *) (&local_read_buffer_1_[n_handle]), domain_len);
    n_handle += domain_len;
    uint16_t port = 0;
    memcpy(&port, &local_read_buffer_1_[n_handle], PORT_LEN);
    n_handle += PORT_LEN;
    boost::endian::big_to_native_inplace(port);
    remote_request_endpoint_ = domain + ":" + std::to_string(port);
  }
  else if ( atyp == IPV6_ATYP )
  {
    LOG(ERROR) << id_ << " IPV6 address currently not supported";
    close_all();
    return;
  }
  else
  {
    LOG(ERROR) << id_ << " unknown address type: " << atyp;
    close_all();
    return;
  }
  
  consume(n_handle, local_read_buffer_1_);
  
  if ( !local_read_buffer_1_.empty())
  {
    LOG(ERROR) << id_ << " invalid message pack(2)";
    close_all();
    return;
  }
  
  msocks::msg_pack(
    remote_request_endpoint_.data(),
    remote_request_endpoint_.size(),
    remote_write_buffer_.data(),
    secret_
  );
  
  LOG(INFO) << id_ << " connect to: " << remote_request_endpoint_;
  auto to_write = msocks::get_pack_size(remote_request_endpoint_.size());
  
  auto remote_write_cb = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " write to remote socket error(3):" << ec.message();
      close_all();
      return;
    }
    handshake_3();
  };
  
  async_write(remote_socket_, buffer(remote_write_buffer_, to_write), remote_write_cb);
  
}

void client_session::handshake_3()
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " read from remote socket error(3):" << ec.message();
      close_all();
      return;
    }
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    handshake_3();
  };
  
  auto write = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      LOG(ERROR) << id_ << " write to local socket error(3):" << ec.message();
      close_all();
      return;
    }
    data_handle();
  };
  
  auto size = remote_read_buffer_1_.size();
  
  if ( size < CryptoPP::SHA256::DIGESTSIZE )
  {
    remote_socket_.async_receive(buffer(remote_read_buffer_0_), read);
    return;
  }
  
  auto origin_digest = msocks::sha256(remote_request_endpoint_);
  
  if ( memcmp(origin_digest.data(), remote_read_buffer_1_.data(), CryptoPP::SHA256::DIGESTSIZE))
  {
    LOG(ERROR) << id_ << " invalid request hashing";
    close_all();
    return;
  }
  
  LOG(INFO) << id_ << " socks5 proxy tunnel established";
  consume(CryptoPP::SHA256::DIGESTSIZE, remote_read_buffer_1_);
  
  if ( !remote_read_buffer_1_.empty())
  {
    LOG(ERROR) << id_ << " invalid message pack(3)";
    close_all();
    return;
  }
  
  local_read_buffer_1_.clear();
  remote_read_buffer_1_.clear();
  
  uint16_t port = remote_ep_.port();
  boost::endian::native_to_big_inplace(port);
  memcpy(local_write_buffer_.data(), "\x05\x00\x00\x01\x00\x00\x00\x00", 8);
  memcpy(local_write_buffer_.data() + 8, &port, PORT_LEN);
  async_write(local_socket_, buffer(local_write_buffer_, 8 + PORT_LEN), write);
}

void client_session::data_handle()
{
  auto session = shared_from_this();
  local_socket_.async_receive(buffer(local_read_buffer_0_), [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    on_local_read();
  });
  
  remote_socket_.async_receive(buffer(remote_read_buffer_0_), [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    on_remote_read();
  });
}

void client_session::on_local_read()
{
  auto session = shared_from_this();
  auto write = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " write to remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    on_remote_write();
  };
  
  msocks::msg_pack(
    local_read_buffer_1_.data(),
    local_read_buffer_1_.size(),
    remote_write_buffer_.data(),
    secret_
  );
  auto to_write = msocks::get_pack_size(local_read_buffer_1_.size());
  async_write(remote_socket_, buffer(remote_write_buffer_, to_write), write);
}

void client_session::on_local_write(std::size_t n_write)
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    on_remote_read();
  };
  
  consume(msocks::get_pack_size(n_write), remote_read_buffer_1_);
  if ( !remote_read_buffer_1_.empty())
  {
    on_remote_read();
  }
  else
  {
    remote_socket_.async_receive(buffer(remote_read_buffer_0_), read);
  }
}

void client_session::on_remote_read()
{
  auto session = shared_from_this();
  
  auto write = [session, this](const error_code &ec, std::size_t n_write)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " write to local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    on_local_write(n_write);
  };
  
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from remote error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    remote_read_buffer_1_.insert(
      remote_read_buffer_1_.end(),
      remote_read_buffer_0_.begin(),
      remote_read_buffer_0_.begin() + n_read
    );
    on_remote_read();
  };
  
  auto n = msocks::msg_unpack(
    remote_read_buffer_1_.data(),
    remote_read_buffer_1_.size(),
    local_write_buffer_.data(),
    secret_
  );
  
  if ( n >= 0 )
  {
    auto to_write = msocks::get_unpack_size(remote_read_buffer_1_.size() - n);
    async_write(local_socket_, buffer(local_write_buffer_, to_write), write);
  }
  else
  {
    remote_socket_.async_receive(buffer(remote_read_buffer_0_), read);
  }
}

void client_session::on_remote_write()
{
  auto session = shared_from_this();
  auto read = [session, this](const error_code &ec, std::size_t n_read)
  {
    if ( ec )
    {
      if ( ec != error::eof && ec != error::operation_aborted )
      {
        LOG(ERROR) << id_ << " read from local error: " << ec.message();
      }
      if ( ec != error::operation_aborted )
        close_all();
      return;
    }
    local_read_buffer_1_.insert(
      local_read_buffer_1_.end(),
      local_read_buffer_0_.begin(),
      local_read_buffer_0_.begin() + n_read
    );
    on_local_read();
  };
  local_read_buffer_1_.clear();
  local_socket_.async_receive(buffer(local_read_buffer_0_), read);
}

void client_session::close_all()
{
  error_code ec;
  local_socket_.close(ec);
  remote_socket_.close(ec);
  timer_.cancel(ec);
}

void client_session::consume(size_t n, std::vector<uint8_t> &target)
{
  if ( n == target.size())
  {
    target.clear();
  }
  else
  {
    std::move(target.begin() + n, target.end(), target.begin());
    while ( n-- > 0 ) target.pop_back();
  }
}

client_session::~client_session()
{
  LOG(INFO)<<id_<<" session released";
}



