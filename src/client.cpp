//
// Created by maxtorm on 18-7-12.
//
#include "client.h"
#include "Encrypt.h"
#include "client_session.h"

#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <glog/logging.h>

using namespace boost::asio;
using boost::system::error_code;
using namespace ip;

client::client(
  tcp::endpoint remote_ep,
  std::string_view secret
) :
  basic_server(tcp::endpoint(make_address_v4("127.0.0.1"), 1090), secret),
  remote_ep_(std::move(remote_ep))
{
}

void client::start()
{
  acceptor_.async_accept([this](const error_code &ec, tcp::socket ps)
                         {
                           on_accept(ec, std::move(ps));
                         });

  basic_server::run();
}

void client::on_accept(const error_code &ec, tcp::socket ps)
{

  if (ec)
  {
    LOG(FATAL)<<"on client accepting connection error: "<<ec.message();
  }
  std::shared_ptr<client_session> session(new client_session(std::move(ps),secret_,remote_ep_));
  session->run();
  acceptor_.async_accept([this](const error_code &ec, tcp::socket ps)
                         {
                           on_accept(ec, std::move(ps));
                         });
}
